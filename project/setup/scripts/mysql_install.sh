#! /bin/bash
# Install MySQL & required installation dependencies
echo "Installing debconf-utils"
apt-get install debconf-utils -y
echo "Setting mysql root password"
echo "mysql-server mysql-server/root_password password root" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password root" | debconf-set-selections
echo "Installing MySQL"
apt-get install mysql-server -y
