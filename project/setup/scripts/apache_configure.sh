#! /bin/bash
# Enable apache mods
echo "Enabling mod-rewrite"
a2enmod rewrite

sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini

echo "Disabling default site"
a2dissite 000-default
echo "Removing default index.html page"
rm /var/www/html/index.html
echo "Restarting apache"
service apache2 restart
