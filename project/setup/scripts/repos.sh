#! /bin/bash
echo "Installing python-software-properties and build-essential"
apt-get install python-software-properties build-essential -y
echo "Adding ondrej apache/php repositories"
add-apt-repository ppa:ondrej/apache
add-apt-repository ppa:ondrej/php5 -y
echo "Updating apt-get repositories again"
apt-get update -y
