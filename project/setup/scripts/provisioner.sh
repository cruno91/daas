#! /bin/bash
echo "Provisioning virtual machine..."

sh /var/www/html/setup/scripts/tz.sh
sh /var/www/html/setup/scripts/update.sh
sh /var/www/html/setup/scripts/repos.sh
sh /var/www/html/setup/scripts/mysql_install.sh
sh /var/www/html/setup/scripts/mysql_configure.sh
sh /var/www/html/setup/scripts/apache_php_install.sh
sh /var/www/html/setup/scripts/apache_configure.sh
sh /var/www/html/setup/scripts/apache_add_site.sh
sh /var/www/html/setup/scripts/composer_install.sh
sh /var/www/html/setup/scripts/drush_install.sh

echo "=================Provisioning Complete!=================================="

