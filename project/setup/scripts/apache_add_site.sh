#! /bin/bash
# Create vhost file
echo "Copying vhost to sites-available"
cp /var/www/html/setup/files/daas_vhost /etc/apache2/sites-available/www.daas.vm.conf
echo "Enabling nyustern"
a2ensite www.daas.vm
echo "Restarting apache"
service apache2 restart
