# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure(2) do |config|

  module OS
    def OS.windows?
        (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
    end

    def OS.mac?
        (/darwin/ =~ RUBY_PLATFORM) != nil
    end

    def OS.unix?
        !OS.windows?
    end

    def OS.linux?
        OS.unix? and not OS.mac?
    end
  end

  config.vm.box = "precise64"

  config.vm.box_url = "http://files.vagrantup.com/precise64.box"

  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true

  config.vm.define "daas" do |daas|
    daas.vm.hostname = "www.daas.vm"
    daas.vm.network :private_network, ip: "172.22.22.52"
    is_windows_host = "#{OS.windows?}"
    if OS.windows?
      puts "Vagrant launched from windows."
      daas.vm.synced_folder "./project", "/var/www/html/", type: "smb"
     elsif OS.mac?
      puts "Vagrant launched from mac."
      daas.vm.synced_folder "./project", "/var/www/html/", type: "nfs"
     elsif OS.unix?
      puts "Vagrant launched from unix."
      daas.vm.synced_folder "./project", "/var/www/html/", type: "nfs"
     elsif OS.linux?
      puts "Vagrant launched from linux."
      daas.vm.synced_folder "./project", "/var/www/html/", type: "nfs"
    else
      puts "Vagrant launched from mac/unix/linux/unknown platform."
      daas.vm.synced_folder "./project", "/var/www/html/", type: "nfs"
    end

    daas.vm.provider "virtualbox" do |vm|
      vm.customize ["modifyvm", :id, "--memory", "2048"]
      vm.customize ["modifyvm", :id, "--cpus", "2"]
      vm.customize ["modifyvm", :id, "--cpuexecutioncap", "90"]
    end

    daas.vm.provision "shell" do |script|
      script.path = "project/setup/scripts/provisioner.sh"
    end
  end

  config.vm.provision :hostmanager

end
